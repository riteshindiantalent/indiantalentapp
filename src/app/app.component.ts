import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController , } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { CallNumber } from '@ionic-native/call-number';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any,logo: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public menuCtl:MenuController, private callNumber: CallNumber,public sBar:StatusBar) {
    this.initializeApp();
    sBar.overlaysWebView(true);
    sBar.show();
    sBar.backgroundColorByHexString('#2868cc'); 
    sBar.styleLightContent();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage  ,logo:'home'},     
      // { title: 'Winner List', component: HomePage ,logo:'copy'},
      { title: 'School Login', component: "SchoolLoginPage" ,logo:'lock'},
      { title: 'Student Result', component: "ResultPage" ,logo:'paper'},
      { title: 'Become Coordinator', component: "BcoordinatorPage" ,logo:'person'},
      { title: 'Exam Pattern', component: "ExampatternPage" ,logo:'paper'},
      { title: 'Study Material', component: "StudymaterialPage" ,logo:'paper'},
      { title: 'Invite School', component: "InviteSchoolPage" ,logo:'person'},
      { title: 'Contact', component: "ContactPage" ,logo:'contact'}
    ]
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  

  openPage(page) {
    this.menuCtl.close();
    this.nav.setRoot(page.component);
  }

  async call():Promise<any>{
    try{
      await this.callNumber.callNumber('18002669192', true);
    }catch(e){
      console.error(e);
    }
  }
}
