import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,private toast: ToastController) {
  }

  Toast(Message : string){
    let toast = this.toast.create({
    message: Message,
    duration: 2000,
    position: 'bottom'
    });
    toast.present(toast);
}

  schoolLogin(){
    this.navCtrl.push("SchoolLoginPage");
  }
  result(){
    this.navCtrl.push("ResultPage");
  }
  bcoordintor(){
    this.navCtrl.push("BcoordinatorPage");
  }
  study(){
    this.navCtrl.push("StudymaterialPage");
  }

  

}
