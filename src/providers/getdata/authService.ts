import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/ADD/operator/map';

// ---------------------Online Apis

let result = "https://www.indiantalent.org/webservice/result";
let login = "https://www.indiantalent.org/webservice/login";
let invitschool = "https://www.indiantalent.org/webservice/inviteSchool";
let coordinator = "https://www.indiantalent.org/webservice/becomeCoordinator";



@Injectable()
export class AuthService {
private headers:Headers;
  constructor(public http : Http) {    
    this.headers=new Headers();
    this.headers.append('Content-Type', 'application/json; charset=UTF-8');
    this.headers.append('Accept','application/json');
  }

  // function made for login
  resullt(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(result, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });
  }

  Login(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(login, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
          console.log(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });
  }

  inviteschool(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(invitschool, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

  coordinator(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(coordinator, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }
   

}